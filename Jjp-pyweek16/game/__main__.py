import pygame
import sys
import data
import copy

from random import randint
from pygame.locals import *

# My stuff
import units
from player import Player
from level import Level
from sound import SoundSystem
from statusbar import StatusBar
from constants import *

def read_levels():
    level_data = data.load('level_data.txt', 'r')
    
    all_levels = []
    current_level = []
    reading = False
    
    for line in level_data:
        line = line.strip()
        
        if line[:2] == '//': # Comments
            continue
        elif line.count("MAZE") == 1 or line.count("BOSS") == 1:
            reading = True
        
        if reading: # Currently reading a field level
            current_level.append(line)
            
            if line.count("ENDSCRIPT") == 1: # An end marker was found
                reading = False
                new_level = Level(current_level[:])
                all_levels.append(new_level)
                
                current_level = []
    
    return all_levels
    
def end():
    pygame.quit()
    sys.exit()

def main():
    pygame.init()
    display = pygame.display.set_mode((GAMEWIDTH, GAMEHEIGHT))
    pygame.display.set_caption("Evil Squared")
    clock = pygame.time.Clock()
    
    sound_player = SoundSystem()
    
    levels = read_levels()
    level_index = 0
    game_state = TITLE
    
    while True:
        if game_state == TITLE:
            result, level_picked = menu_screen(display, clock, len(levels), sound_player)
            
            if result == START:
                game_state = GAMEPLAY
                level_index = level_picked - 1
            elif result == QUITGAME:
                game_state = EXIT
        
        elif game_state == GAMEPLAY:
            current_level = copy.deepcopy(levels[level_index])
            result = level_screen(display, clock, current_level, sound_player)
            
            if result == WIN:
                if level_index == len(levels) - 1:
                    game_complete_screen(display, clock, current_level, sound_player)
                    game_state = TITLE
                else:
                    current_level = levels[level_index]
                    level_index = (level_index + 1) % len(levels)
            elif result == QUITLEVEL:
                game_state = TITLE
                level_index = 0
            # If the player lost, do nothing; it will loop back and start 
            # the level again
            else:
                pass
                
        else: # Exit the game
            end()
            
def menu_screen(display, clock, total_levels, sound_player):
    font_file = data.filepath('FreeUniversal-Bold.ttf')
    title_font = pygame.font.Font(font_file, 48)
    button_font = pygame.font.Font(font_file, 24)
    
    choice = 0
    level_choice = 1
    options = [("Start Game", START), 
               ("Quit Game", QUITGAME)]
    
    while True:
        # Events
        for event in pygame.event.get():
            if event.type == QUIT:
                end()
            
            elif event.type == KEYUP:
                if event.key == K_UP:
                    choice = (choice - 1) % len(options)
                elif event.key == K_DOWN:
                    choice = (choice + 1) % len(options)
                elif event.key == K_LEFT and level_choice > 1:
                    level_choice -= 1
                elif event.key == K_RIGHT and level_choice < total_levels:
                    level_choice += 1
                elif event.key == K_RETURN or event.key == K_SPACE:
                    chosen_option = options[choice]
                    return chosen_option[1], level_choice
                elif event.key == K_m:
                    sound_player.can_play = not sound_player.can_play
        
        # Draw
        display.fill(BLACK)
        
        title_display = title_font.render("Evil Squared", True, RED)
        title_rect = title_display.get_rect()
        title_rect.center = (380, 50)
        display.blit(title_display, title_rect)
        
        for i in range(len(options)):
            button_color = YELLOW if choice == i else WHITE
            
            button_display = button_font.render(options[i][0], True, button_color)
            button_rect = title_display.get_rect()
            button_rect_y = (i * 40) + 250
            button_rect.width = 180 # For some reason, the rects are wider than the text
            button_rect.center = (400, button_rect_y)
            display.blit(button_display, button_rect)
            
        level_select_display = button_font.render(
            "Selected level: " + str(level_choice) + " (Left or right arrows to change)",
            True, WHITE)
        level_select_rect = level_select_display.get_rect()
        level_select_rect.center = (400, 400)
        display.blit(level_select_display, level_select_rect)
        
        sound_option = "On" if sound_player.can_play else "Off"
        
        sound_display = button_font.render(
            "Sound: " + sound_option + " (Press M to toggle)", True, WHITE)
        sound_rect = sound_display.get_rect()
        sound_rect.center = (400, 460)
        display.blit(sound_display, sound_rect)
        
        help_text = "Up and down arrows - Select option | Enter or Space - Confirm"
        help_display = button_font.render(help_text, True, WHITE)
        help_text_rect = help_display.get_rect()
        help_text_rect.center = (400, 580)
        display.blit(help_display, help_text_rect)
        
        pygame.display.update()
        clock.tick(FPS)

def level_screen(display, clock, level, sound_player):
    level_won = False
    level_lose = False
    
    player = Player(level)
    status = StatusBar()
    time_left = level.initial_time
    time_running = True
    tick = 0
    
    # Put this here for msg:start script actions to work correctly
    status.update(level, time_left, clock.get_fps())
    status.update_boss(player, level)
    
    while True:
        # Events
        for event in pygame.event.get():
            if event.type == QUIT:
                end()
                
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pause_result = pause_screen(display, clock, sound_player)
                    if pause_result != RESUME:
                        return pause_result
                    player.freeze() # Reset the state of the player's input
                    
                elif event.key == K_RETURN:
                    if level_won:
                        return WIN
                    elif level_lose:
                        return LOSE
                    
                elif not level_won and not level_lose:
                    player.read_input(event.key, event.type)
                    
            elif event.type == KEYUP:
                player.read_input(event.key, event.type)
        
        # It's possible that this is really out of order...
        player.update_pos(level)
        player.update_bullets(level)
        level.update_breakables(player.rect, tick)
        level.tick(tick) # Updates turrets and bullets
        level.tick_boss(player, tick)
        
        if level.check_for_pitfall(player.rect) and not player.dead:
            player.dead = True
            player.play_hit = True
        if level.check_for_bullets(player.rect) and not player.dead:
            player.dead = True
            player.play_hit = True # Yay sounds
        if level.check_for_goal(player.rect) and not level_won:
            level_won = True
            sound_player.flags["level-clear"] = True
        
        if tick == 0 and time_left >= 0 and time_running:
            time_left -= 1
        tick = (tick + 1) % FPS
        
        if level_won:
            player.freeze()
            time_running = False
            status.current_message = "Level complete! Press ENTER to continue."
        elif player.dead or time_left < 0:
            player.freeze()
            time_running = False
            level_lose = True
            
            if player.dead:
                player.health = 0
                status.current_message = "Ouch! Press ENTER to try again."
            elif time_left < 0:
                status.current_message = "Time over! Press ENTER to try again."
        
        if level_won or level_lose:
            status.override_level_msg = True
        
        status.update(level, time_left, clock.get_fps())
        status.update_boss(player, level)
        
        # Play sounds
        player.sound(sound_player)
        level.sound(sound_player)
        sound_player.play_sounds()
        
        # Draw
        display.fill(BLACK)
        
        level.draw(display)
        player.draw(display)
        status.draw(display)
        
        pygame.display.update()
        clock.tick(FPS)
    
def pause_screen(display, clock, sound_player):
    font_file = data.filepath('FreeUniversal-Bold.ttf')
    title_font = pygame.font.Font(font_file, 48)
    button_font = pygame.font.Font(font_file, 24)
    
    choice = 0
    options = [("Resume Game", RESUME),
               ("Restart Level", LOSE),
               ("Quit Game", QUITLEVEL)]
    
    while True:
        # Events
        for event in pygame.event.get():
            if event.type == QUIT:
                end()
            
            elif event.type == KEYUP:
                if event.key == K_UP:
                    choice = (choice - 1) % len(options)
                elif event.key == K_DOWN:
                    choice = (choice + 1) % len(options)
                elif event.key == K_RETURN or event.key == K_SPACE:
                    chosen_option = options[choice]
                    return chosen_option[1]
                elif event.key == K_m:
                    sound_player.can_play = not sound_player.can_play
        
        # Draw
        display.fill(BLACK)
        
        title_display = title_font.render("Paused", True, WHITE)
        title_rect = title_display.get_rect()
        title_rect.center = (400, 250)
        display.blit(title_display, title_rect)
        
        for i in range(len(options)):
            button_color = YELLOW if choice == i else WHITE
            
            button_display = button_font.render(options[i][0], True, button_color)
            button_rect = title_display.get_rect()
            button_rect_y = (i * 40) + 350
            button_rect.width = 180 # For some reason, the rects are wider than the text
            button_rect.center = (400, button_rect_y)
            display.blit(button_display, button_rect)
        
        sound_option = "On" if sound_player.can_play else "Off"
        
        sound_display = button_font.render(
            "Sound: " + sound_option + " (Press M to toggle)", True, WHITE)
        sound_rect = sound_display.get_rect()
        sound_rect.center = (400, 460)
        display.blit(sound_display, sound_rect)
        
        help_text = "Up and down arrows - Select option | Enter or Space - Confirm"
        help_display = button_font.render(help_text, True, WHITE)
        help_text_rect = help_display.get_rect()
        help_text_rect.center = (400, 580)
        display.blit(help_display, help_text_rect)
        
        pygame.display.update()
        clock.tick(FPS)

def game_complete_screen(display, clock, total_levels, sound_player):
    font_file = data.filepath('FreeUniversal-Bold.ttf')
    big_title_font = pygame.font.Font(font_file, 72)
    text_font = pygame.font.Font(font_file, 24)
    
    ignore_keyup = True # Otherwise, the screen just flashes by
    
    while True:
        # Events
        for event in pygame.event.get():
            if event.type == QUIT:
                end()
            
            elif event.type == KEYUP:
                if ignore_keyup:
                    ignore_keyup = False
                elif event.key == K_RETURN or event.key == K_SPACE:
                    return

        display.fill(BLACK)
        
        congrats_display = big_title_font.render("Congratulations!", True, YELLOW)
        congrats_rect = congrats_display.get_rect()
        congrats_rect.center = (400, 100)
        display.blit(congrats_display, congrats_rect)
        
        more_words = "You completed the final level! Thank you for playing!"
        more_words_display = text_font.render(more_words, True, WHITE)
        more_words_rect = more_words_display.get_rect()
        more_words_rect.center = (400, 300)
        display.blit(more_words_display, more_words_rect)
        
        help_text = "Press Enter or Space to continue"
        help_display = text_font.render(help_text, True, WHITE)
        help_text_rect = help_display.get_rect()
        help_text_rect.center = (400, 580)
        display.blit(help_display, help_text_rect)

        pygame.display.update()
        clock.tick(FPS)
