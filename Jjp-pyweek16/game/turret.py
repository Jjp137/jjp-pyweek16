# Stuff for the turrets
import pygame
from pygame.locals import *

import units
from bullet import Bullet
from constants import *

class Turret(object):
    def __init__(self, tile_x, tile_y, facing):
        self.tile_loc = (tile_x, tile_y)
        self.facing = facing
        self.tick_list = [0]
        self.bullets = []
        self.bullets_to_del = []
        
        self.rect = units.get_tile_rect(self.tile_loc[0], self.tile_loc[1])
        self.has_shot = False
        
    def tick(self, tick):
        # Do cleanup first
        for bullet in self.bullets_to_del:
            self.bullets.remove(bullet)

        self.bullets_to_del = []
        
        # Update each bullet's position
        for bullet in self.bullets:
            bullet.update_pos()
        
        # Spawn a bullet, with offsets, if the time is right
        if tick in self.tick_list:
            if self.facing == LEFTTURRET:
                bullet_x = self.rect.topleft[0] - BULLETSIZE
                bullet_y = self.rect.topleft[1] + 5
                
                self.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED, 0))
            elif self.facing == RIGHTTURRET:
                bullet_x = self.rect.topright[0]
                bullet_y = self.rect.topright[1] + 5
                
                self.bullets.append(Bullet(bullet_x, bullet_y, BULLETSPEED, 0))
            elif self.facing == UPTURRET:
                bullet_x = self.rect.topleft[0] + 5
                bullet_y = self.rect.topleft[1] - BULLETSIZE
                
                self.bullets.append(Bullet(bullet_x, bullet_y, 0, -BULLETSPEED))
            elif self.facing == DOWNTURRET:
                bullet_x = self.rect.bottomleft[0] + 5
                bullet_y = self.rect.bottomleft[1]
                
                self.bullets.append(Bullet(bullet_x, bullet_y, 0, BULLETSPEED))
            
            self.has_shot = True # For sounds
                
