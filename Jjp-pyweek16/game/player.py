# Stuff for the player class
import pygame
from pygame.locals import *

import data
import units
from bullet import Bullet
from constants import *

class Player(object):
    def __init__(self, level, can_shoot=False):
        self.x = level.start_pos[0]
        self.y = level.start_pos[1]
        self.rect = pygame.Rect(self.x, self.y, PLAYERSIZE, PLAYERSIZE)
        
        self.moving_up = False
        self.moving_down = False
        self.moving_left = False
        self.moving_right = False
        self.running = False
        
        # The player can only shoot during boss levels
        if level.level_type == BOSS:
            self.can_shoot = True
        else:
            self.can_shoot = False
        
        self.shooting = False
        self.shoot_cooldown = 0
        self.health = 3
        self.damage_effect = 0
        self.bullets = []
        self.bullets_to_del = []
        
        self.dead = False
        
        # Sound flags
        self.play_shoot = False
        self.play_hit = False

    def read_input(self, key, key_state):
        if self.dead:
            return
        
        if key == K_UP:
            self.moving_up = True if key_state == KEYDOWN else False
        if key == K_DOWN:
            self.moving_down = True if key_state == KEYDOWN else False
        if key == K_LEFT:
            self.moving_left = True if key_state == KEYDOWN else False
        if key == K_RIGHT:
            self.moving_right = True if key_state == KEYDOWN else False
        if key == K_LSHIFT or key == K_RSHIFT:
            self.running = True if key_state == KEYDOWN else False
        if key == K_SPACE:
            self.shooting = True if key_state == KEYDOWN else False
    
    def freeze(self):
        self.moving_up = False
        self.moving_down = False
        self.moving_left = False
        self.moving_right = False
        self.running = False
        self.shooting = False
        
    def update_pos(self, level):
        if self.dead:
            return
        
        delta_x = 0
        delta_y = 0
        
        if self.moving_left:
            delta_x += -RUNRATE if self.running else -MOVERATE
        if self.moving_right:
            delta_x += RUNRATE if self.running else MOVERATE
        if self.moving_up:
            delta_y += -RUNRATE if self.running else -MOVERATE
        if self.moving_down:
            delta_y += RUNRATE if self.running else MOVERATE
            
        new_x = self.x + delta_x
        new_y = self.y + delta_y
        
        # Check each axis independently
        x_rect = pygame.Rect(new_x, self.y, PLAYERSIZE, PLAYERSIZE)
        y_rect = pygame.Rect(self.x, new_y, PLAYERSIZE, PLAYERSIZE)
        both_rect = pygame.Rect(new_x, new_y, PLAYERSIZE, PLAYERSIZE)
        
        x_adjusted = False
        y_adjusted = False
        
        # Code for collision checking with the level
        while level.check_for_walls(x_rect):
            x_adjusted = True
            # Keep adjusting the position until the player is flush with
            # the wall            
            if new_x > self.x:
                new_x -= 1
                x_rect.x -= 1
            elif new_x < self.x:
                new_x += 1
                x_rect.x += 1
        
        while level.check_for_walls(y_rect):
            y_adjusted = True
            
            if new_y > self.y:
                new_y -= 1
                y_rect.y -= 1
            elif new_y < self.y:
                new_y += 1
                y_rect.y += 1
                
        # If we didn't detect any collisions when moving the axes
        # independently, there might still be collisions when we move both
        # the x and y together.
        if not x_adjusted and not y_adjusted:
            while level.check_for_walls(both_rect):
                if new_x > self.x:
                    new_x -= 1
                    both_rect.x -= 1
                elif new_x < self.x:
                    new_x += 1
                    both_rect.x += 1
                        
                if new_y > self.y:
                    new_y -= 1
                    both_rect.y -= 1
                elif new_y < self.y:
                    new_y += 1
                    both_rect.y += 1
            
        self.x = new_x
        self.y = new_y
        self.rect = pygame.Rect(self.x, self.y, PLAYERSIZE, PLAYERSIZE)
    
    def update_bullets(self, level):
        if not self.can_shoot:
            return
        
        # Do cleanup first
        for bullet in self.bullets_to_del:
            if bullet in self.bullets: # In case the player somehow dies at the right tick
                self.bullets.remove(bullet)

        self.bullets_to_del = []
        
        # Update each bullet's position
        for bullet in self.bullets:
            bullet.update_pos()
        
        # Check if any of the player's bullets are in a wall of some sort
        for bullet in self.bullets:
            if level.check_for_walls(bullet.rect):
                bullet.active = False
                self.bullets_to_del.append(bullet)
        
        # Only shoot if the cooldown has expired
        if self.shoot_cooldown == 0 and self.shooting:
            bullet_x = self.rect.topright[0]
            bullet_y = self.rect.topright[1] + 2
            # Make the player's bullets faster and friendly
            self.bullets.append(Bullet(bullet_x, bullet_y, BULLETSPEED * 2, 0, True))
            self.shoot_cooldown = 5
            # Play the sound that occurs when a bullet is shot
            self.play_shoot = True
            
        elif self.shoot_cooldown > 0:
            self.shoot_cooldown -= 1
            
    def sound(self, sound_player):
        sound_player.flags["player-hit"] = self.play_hit
        sound_player.flags["player-shoot"] = self.play_shoot
        
        self.play_shoot = False
        self.play_hit = False
    
    def draw(self, display):
        if self.damage_effect > 0:
            self.damage_effect -= 1
        
        if self.dead and self.rect.width > 0 and self.rect.height > 0:
            self.rect.inflate_ip(-1, -1)
            # Keep the animation centered
            if self.rect.width % 2 == 0 and self.rect.height % 2 == 0:
                self.rect.centerx += 1
                self.rect.centery += 1
                
        player_color = YELLOW if self.damage_effect > 0 else BLUE
        
        if self.rect.width > 0 and self.rect.height > 0:
            pygame.draw.rect(display, player_color, self.rect)
        
        # Draw the player's bullets
        for bullet in self.bullets:
            bullet.draw(display)
        
