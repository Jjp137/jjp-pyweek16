# Stuff for the level class
import pygame
from pygame.locals import *

import units
from bullet import Bullet
from turret import Turret
from boss import Boss
from constants import *

class Level(object):
    def __init__(self, data):
        self.level_type = None
        self.tiles = []
        self.border_rects = []
        
        self.floor_num = None
        self.initial_time = None
        
        # (x, y); These are in pixels
        self.start_pos = None
        self.goal_pos = None
        self.boss_pos = None
        
        self.about_to_break = {} # (tile_x, tile_y) -> tick
        self.turrets = []
        self.messages = {} # time -> message
        
        # For boss fights
        self.boss = None
        
        # Sounds
        self.play_breakable = False
        self.play_turret = False
        
        # Create the borders
        self.border_rects.append(pygame.Rect(0, 0, GAMEWIDTH, TILESIZE))
        self.border_rects.append(pygame.Rect(0, LEVELHEIGHT + BORDERSIZE, GAMEWIDTH, TILESIZE))
        self.border_rects.append(pygame.Rect(0, 0, TILESIZE, LEVELHEIGHT + BORDERSIZE))
        self.border_rects.append(pygame.Rect(LEVELWIDTH + BORDERSIZE, 0, TILESIZE, LEVELHEIGHT + BORDERSIZE))
        
        # Figure out the level type
        if data[0] == "MAZE":
            self.level_type = MAZE
        elif data[0] == "BOSS":
            self.level_type = BOSS

        # Extract the tile data
        start = data.index(self.level_type) + 1
        end = data.index("ENDTILES")
        tile_count = 0
        
        for i in range(start, end):
            self.tiles.append(list(data[i]))
            tile_count += len(data[i])
            
        assert tile_count == 600, "The level is corrupt."
            
        # Find the various special tiles
        for y in range(len(self.tiles)):
            for x in range(len(self.tiles[y])):
                if self.tiles[y][x] == START:
                    start_x = units.tiles_to_pixels(x)
                    start_y = units.tiles_to_pixels(y)
                    self.start_pos = (start_x, start_y)
                elif self.tiles[y][x] == GOAL:
                    goal_x = units.tiles_to_pixels(x)
                    goal_y = units.tiles_to_pixels(y)
                    self.goal_pos = (goal_x, goal_y)
                elif self.tiles[y][x] == BOSSSPAWN:
                    boss_x = units.tiles_to_pixels(x)
                    boss_y = units.tiles_to_pixels(y)
                    self.boss_pos = (boss_x, boss_y)
                elif self.tiles[y][x] == LEFTTURRET:
                    self.turrets.append(Turret(x, y, LEFTTURRET))
                elif self.tiles[y][x] == RIGHTTURRET:
                    self.turrets.append(Turret(x, y, RIGHTTURRET))
                elif self.tiles[y][x] == UPTURRET:
                    self.turrets.append(Turret(x, y, UPTURRET))
                elif self.tiles[y][x] == DOWNTURRET:
                    self.turrets.append(Turret(x, y, DOWNTURRET))
           
        # If this is a boss level, create the boss
        if self.level_type == BOSS:
            self.boss = Boss(self.boss_pos)
                    
        # Read attributes
        start = data.index("ENDTILES") + 1
        end = data.index("SCRIPT")
        
        for i in range(start, end):
            attribute = data[i].split(':')
            
            if attribute[0] == "FLOOR":
                self.floor_num = int(attribute[1])
            elif attribute[0] == "TIME":
                self.initial_time = int(attribute[1])
            elif attribute[0] == "BOSSHP" and self.level_type == BOSS:
                self.boss.initial_health = int(attribute[1])
                self.boss.health = int(attribute[1])
                
        # Read the script and apply its effects
        start = data.index("SCRIPT") + 1
        end = data.index("ENDSCRIPT")
        
        for i in range(start, end):
            action = data[i].split(':')
            
            if action[0] == "msg":
                if action[1] == "start":
                    self.messages[self.initial_time] = action[2]
                else:
                    self.messages[int(action[1])] = action[2]
                    
            elif action[0] == "turret":
                turret_x = int(action[1])
                turret_y = int(action[2])
                
                for turret in self.turrets:
                    if turret.tile_loc == (turret_x, turret_y):
                        turret.tick_list.remove(0) # Clear the default tick
                        
                        for tick in action[3].split(','):
                            turret.tick_list.append(int(tick))
            
            elif action[0] == "athealth" and self.level_type == BOSS:
                pattern = self.boss.all_patterns[action[2]]
                if action[1] == "full":
                    self.boss.script[self.boss.initial_health] = pattern
                    self.boss.pattern = pattern
                else:
                    self.boss.script[int(action[1])] = pattern
    
    def check_for_walls(self, rect):
        tile_left = units.pixels_to_tiles(rect.left)
        tile_right = units.pixels_to_tiles(rect.right)
        tile_top = units.pixels_to_tiles(rect.top)
        tile_bottom = units.pixels_to_tiles(rect.bottom)
        
        # Border checking
        if rect.collidelist(self.border_rects) != -1:
            return True
            
        # Wall checking
        possible_walls = []
        collidables = (WALL, LEFTTURRET, RIGHTTURRET, UPTURRET, DOWNTURRET)
        
        if self.tiles[tile_top][tile_left] in collidables:
            possible_walls.append(units.get_tile_rect(tile_left, tile_top))
        
        # 30 = right edge, 20 = bottom edge
        # Check for those first, or else the index will be out of bounds
        if tile_right == 30 or self.tiles[tile_top][tile_right] in collidables:
            possible_walls.append(units.get_tile_rect(tile_right, tile_top))
            
        if tile_bottom == 20 or self.tiles[tile_bottom][tile_left] in collidables:
            possible_walls.append(units.get_tile_rect(tile_left, tile_bottom))
            
        if tile_right == 30 or tile_bottom == 20 or \
            self.tiles[tile_bottom][tile_right] in collidables:
            possible_walls.append(units.get_tile_rect(tile_right, tile_bottom))
            
        if len(possible_walls) == 0:
            return False
        else:
            return rect.collidelist(possible_walls) != -1
            
    def check_for_pitfall(self, rect):
        tile_left = units.pixels_to_tiles(rect.left)
        tile_right = units.pixels_to_tiles(rect.right)
        tile_top = units.pixels_to_tiles(rect.top)
        tile_bottom = units.pixels_to_tiles(rect.bottom)
        
        possible_pits = []
        
        if self.tiles[tile_top][tile_left] == PIT:
            if units.get_tile_rect(tile_left, tile_top) not in possible_pits:
                possible_pits.append(units.get_tile_rect(tile_left, tile_top))
        
        # 30 = right edge, 20 = bottom edge
        # Check for those first, or else the index will be out of bounds
        if not tile_right == 30 and self.tiles[tile_top][tile_right] == PIT:
            if units.get_tile_rect(tile_right, tile_top) not in possible_pits:
                possible_pits.append(units.get_tile_rect(tile_right, tile_top))
            
        if not tile_bottom == 20 and self.tiles[tile_bottom][tile_left] == PIT:
            if units.get_tile_rect(tile_left, tile_bottom) not in possible_pits:
                possible_pits.append(units.get_tile_rect(tile_left, tile_bottom))
            
        if not tile_right == 30 and not tile_bottom == 20 and \
            self.tiles[tile_bottom][tile_right] == PIT:
            if units.get_tile_rect(tile_right, tile_bottom) not in possible_pits:
                possible_pits.append(units.get_tile_rect(tile_right, tile_bottom))
        
        if len(possible_pits) == 0:
            return False

        total_area = 0
        
        for pit in possible_pits:
            total_area += self.rect_intersect_area(pit, rect)
        
        if total_area > 200: # More than 50% of the player's area
            return True
        else:
            return False
            
    def check_for_bullets(self, rect):
        for turret in self.turrets:
            for bullet in turret.bullets:
                if bullet.active and not bullet.friendly \
                and rect.colliderect(bullet.rect):
                    return True
                    
        return False
        
    def check_for_goal(self, rect):
        goal_rect = pygame.Rect(self.goal_pos[0], self.goal_pos[1], TILESIZE, TILESIZE)
        
        return rect.colliderect(goal_rect)
    
    def update_breakables(self, rect, tick):
        tile_left = units.pixels_to_tiles(rect.left)
        tile_right = units.pixels_to_tiles(rect.right)
        tile_top = units.pixels_to_tiles(rect.top)
        tile_bottom = units.pixels_to_tiles(rect.bottom)
        
        # Check if each possible tile actually overlaps instead of meeting
        # at the edges
        collides_topleft = rect.colliderect(units.get_tile_rect(tile_left, tile_top))
        collides_topright = rect.colliderect(units.get_tile_rect(tile_right, tile_top))
        collides_bottomleft = rect.colliderect(units.get_tile_rect(tile_left, tile_bottom))
        collides_bottomright = rect.colliderect(units.get_tile_rect(tile_right, tile_bottom))
        
        # Offset it by -1 so that the tile doesn't fall on the same tick
        break_tick = (tick - 1) % FPS
        
        if self.tiles[tile_top][tile_left] == BREAKABLE:
            if (tile_left, tile_top) not in self.about_to_break and collides_topleft:
                self.about_to_break[(tile_left, tile_top)] = break_tick
        
        # 30 = right edge, 20 = bottom edge
        # Check for those first, or else the index will be out of bounds
        if not tile_right == 30 and self.tiles[tile_top][tile_right] == BREAKABLE:
            if (tile_right, tile_top) not in self.about_to_break and collides_topright:
                self.about_to_break[(tile_right, tile_top)] = break_tick
            
        if not tile_bottom == 20 and self.tiles[tile_bottom][tile_left] == BREAKABLE:
            if (tile_left, tile_bottom) not in self.about_to_break and collides_bottomleft:
                self.about_to_break[(tile_left, tile_bottom)] = break_tick
            
        if not tile_right == 30 and not tile_bottom == 20 and \
            self.tiles[tile_bottom][tile_right] == BREAKABLE:
            if (tile_right, tile_bottom) not in self.about_to_break and collides_bottomright:
                self.about_to_break[(tile_right, tile_bottom)] = break_tick
        
    def rect_intersect_area(self, a, b):
        intersect_left = max(a.left, b.left)
        intersect_right = min(a.right, b.right)
        # The y-axis is flipped, so flip the max and min
        intersect_top = max(a.top, b.top)
        intersect_bottom = min(a.bottom, b.bottom)
                
        if intersect_left < intersect_right and intersect_top < intersect_bottom:
            return (intersect_right - intersect_left) * (intersect_bottom - intersect_top)
        else:
            return 0
        
    def tick(self, tick):
        # Handle any tiles that are supposed to break this turn
        broken_tiles = []
        
        for coords in self.about_to_break.iterkeys():
            if self.about_to_break[coords] == tick:
                self.tiles[coords[1]][coords[0]] = PIT
                broken_tiles.append(coords)
                
                # Let the sound play
                self.play_breakable = True
        
        # Can't delete while iterating, so do this
        for tile in broken_tiles:
            del self.about_to_break[tile]
            
        # Check if a turret should fire out a bullet
        for turret in self.turrets:
            turret.tick(tick)
            
            if turret.has_shot:
                self.play_turret = True
                turret.has_shot = False
            
            # Check for any bullets that are touching a wall and don't render
            # them.
            for bullet in turret.bullets:
                if self.check_for_walls(bullet.rect):
                    bullet.active = False
                    turret.bullets_to_del.append(bullet)
    
    def tick_boss(self, player, tick):
        if self.level_type != BOSS:
            return
        
        if not self.boss.dead:
            self.boss.update_pos(tick)
            self.boss.update_boss_bullets(player, self, tick)
            self.boss.check_player_bullets(player)
            
            # Close the level's gap on defeat
            if self.boss.dead:
                for y in range(len(self.tiles)):
                    for x in range(len(self.tiles[y])):
                        if self.tiles[y][x] == PIT:
                            self.tiles[y][x] = NORMAL
    
    def sound(self, sound_player):
        if self.level_type == BOSS:
            self.boss.sound(sound_player)
        
        sound_player.flags["enemy-shoot"] = self.play_turret
        sound_player.flags["tile-break"] = self.play_breakable
        
        self.play_breakable = False
        self.play_turret = False
        
    def draw(self, display):
        # Draw the borders
        for rect in self.border_rects:
            pygame.draw.rect(display, BROWN, rect)
        
        # Draw the tiles
        for y in range(len(self.tiles)):
            for x in range(len(self.tiles[y])):
                pixel_y = units.tiles_to_pixels(y)
                pixel_x = units.tiles_to_pixels(x)
                tile_rect = pygame.Rect(pixel_x, pixel_y, TILESIZE, TILESIZE)
                tile_type = self.tiles[y][x]
                
                if tile_type in (NORMAL, START, BOSSSPAWN):
                    pygame.draw.rect(display, GRAY, tile_rect)
                elif tile_type == WALL:
                    pygame.draw.rect(display, BROWN, tile_rect)
                elif tile_type == PIT:
                    pygame.draw.rect(display, BLACK, tile_rect)
                elif tile_type == BREAKABLE:
                    if (x, y) in self.about_to_break:
                        pygame.draw.rect(display, DARKERORANGE, tile_rect)
                    else:
                        pygame.draw.rect(display, ORANGE, tile_rect)
                elif tile_type == LEFTTURRET:
                    pygame.draw.rect(display, RED, tile_rect)
                    pygame.draw.polygon(display, BLACK, 
                        (tile_rect.bottomleft, tile_rect.topleft, tile_rect.midright))
                elif tile_type == RIGHTTURRET:
                    pygame.draw.rect(display, RED, tile_rect)
                    pygame.draw.polygon(display, BLACK, 
                        (tile_rect.bottomright, tile_rect.midleft, tile_rect.topright))
                elif tile_type == UPTURRET:
                    pygame.draw.rect(display, RED, tile_rect)
                    pygame.draw.polygon(display, BLACK, 
                        (tile_rect.topleft, tile_rect.topright, tile_rect.midbottom))
                elif tile_type == DOWNTURRET:
                    pygame.draw.rect(display, RED, tile_rect)
                    pygame.draw.polygon(display, BLACK, 
                        (tile_rect.bottomleft, tile_rect.midtop, tile_rect.bottomright))
                elif tile_type == GOAL:
                    pygame.draw.rect(display, YELLOW, tile_rect)
        
        # Draw the turrets' bullets
        for turret in self.turrets:
            for bullet in turret.bullets:
                bullet.draw(display)
        
        # Draw the boss and its bullets
        if self.level_type == BOSS:
            self.boss.draw(display)
