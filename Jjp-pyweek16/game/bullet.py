import pygame
from pygame.locals import *

from constants import *

class Bullet(object):
    def __init__(self, pos_x, pos_y, vel_x, vel_y, friendly=False):
        self.x = pos_x
        self.y = pos_y
        self.vel_x = vel_x
        self.vel_y = vel_y
        self.friendly = friendly
        self.active = True
        
        self.rect = pygame.Rect(self.x, self.y, BULLETSIZE, BULLETSIZE)
    
    def update_pos(self):
        self.x += self.vel_x
        self.y += self.vel_y
        
        self.rect = pygame.Rect(self.x, self.y, BULLETSIZE, BULLETSIZE)
    
    def draw(self, display):
        if not self.active:
            return
        
        color = SKYBLUE if self.friendly else FUCHSIA
        
        pygame.draw.rect(display, color, self.rect)
