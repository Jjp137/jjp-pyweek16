import pygame
from random import randint
from pygame.locals import *

import data
import bulletpattern
from bullet import Bullet
from constants import *

class Boss(object):
    def __init__(self, pos):
        self.pos_x = pos[0]
        self.pos_y = pos[1]
        self.rect = pygame.Rect(pos[0], pos[1], BOSSSIZE, BOSSSIZE)

        self.initial_health = None
        self.health = None
        # health -> pattern
        self.script = {}
        # Current pattern
        self.pattern = None
        
        # Hardcoded list of all patterns ever
        self.all_patterns = {"moving-steps-easy" : bulletpattern.MovingStepsEasy(),
            "moving-const-easy" : bulletpattern.MovingConstantEasy(),
            "moving-const-normal" : bulletpattern.MovingConstantNormal(),
            "moving-const-hard" : bulletpattern.MovingConstantHard(),
            "aimed-easy" : bulletpattern.AimedEasy(),
            "aimed-normal" : bulletpattern.AimedNormal(),
            "shotgun-normal" : bulletpattern.ShotgunNormal(),
            "shotgun-hard" : bulletpattern.ShotgunHard(),
            "berserk" : bulletpattern.Berserk()}
        
        self.bullets = []
        self.bullets_to_del = []
        self.was_damaged = False
        self.dead = False
        
        # Sounds
        self.play_hit = False
        self.play_death = False
        self.play_shoot = False
        
    def update_pos(self, tick):
        self.pattern.assign_new_pos(self, tick)
        
        # Update the rectangle too
        self.rect = pygame.Rect(self.pos_x, self.pos_y, BOSSSIZE, BOSSSIZE)
    
    def update_boss_bullets(self, player, level, tick):
        # Do cleanup first
        for bullet in self.bullets_to_del:
            if bullet in self.bullets: # If the boss somehow dies at the right tick
                self.bullets.remove(bullet)

        self.bullets_to_del = []
        
        # Update each bullet's position
        for bullet in self.bullets:
            bullet.update_pos()
           
        # Tell the pattern to do stuff
        before = len(self.bullets)
        self.pattern.add_new_bullets(self, player, tick)
        
        if len(self.bullets) != before:
            self.play_shoot = True
        
        # Check for collision detection with the player and the level 
        for bullet in self.bullets:
            if level.check_for_walls(bullet.rect):
                bullet.active = False
                self.bullets_to_del.append(bullet)
            
            elif player.rect.colliderect(bullet.rect) and not player.dead:
                player.health -= 1
                player.damage_effect = 3
                
                # Tell sound() to play the getting hit sound
                player.play_hit = True
                
                bullet.active = False
                self.bullets_to_del.append(bullet)
                
                if player.health == 0:
                    player.dead = True
                    player.bullets = [] # Prevent the boss from being killed
                
                break # Prevent more than one bullet at once from damaging the player
    
    def check_player_bullets(self, player):
        # Reset the damage flag for the boss
        self.was_damaged = False
        
        # Don't bother if its dead
        if self.dead:
            return
        
        for bullet in player.bullets:
            if bullet.rect.colliderect(self.rect):
                bullet.active = False
                player.bullets_to_del.append(bullet)
                
                self.was_damaged = True
                self.health -= 1
                self.play_hit = True
                
                # It's possible for two of the player's bullets to hit the
                # boss at the same time, which can cause patterns to be skipped
                if self.health in self.script.keys():
                    self.pattern = self.script[self.health]
        
        if self.health == 0:
            self.dead = True
            self.play_death = True
            self.bullets = [] # Completely erase all bullets
            
    def sound(self, sound_player):
        sound_player.flags["boss-hit"] = self.play_hit
        sound_player.flags["boss-death"] = self.play_death
        sound_player.flags["boss-shoot"] = self.play_shoot
        
        self.play_hit = False
        self.play_death = False
        self.play_shoot = False
    
    def draw(self, display):
        if self.dead and self.rect.width > 0 and self.rect.height > 0:
            self.rect.inflate_ip(-1, -1)
            # Keep the animation centered
            if self.rect.width % 2 == 0 and self.rect.height % 2 == 0:
                self.rect.centerx += 1
                self.rect.centery += 1
        
        boss_color = YELLOW if self.was_damaged else RED
        
        if self.rect.width > 0 and self.rect.height > 0:
            pygame.draw.rect(display, boss_color, self.rect)
            
        for bullet in self.bullets:
            bullet.draw(display)
