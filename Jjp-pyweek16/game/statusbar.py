import pygame
from pygame.locals import *

import data
from constants import *

class StatusBar(object):
    def __init__(self):
        self.font = data.filepath('FreeUniversal-Regular.ttf')
        
        self.message_font = pygame.font.Font(self.font, 16)
        self.status_font = pygame.font.Font(self.font, 20)
        self.small_font = pygame.font.Font(self.font, 10)
        
        self.current_message = "This is where a message goes."
        self.override_level_msg = False # Win or lose messages have priority
        
        self.floor_num = None
        self.time_left = None
        self.fps = None
        
        # Boss stuff
        self.player_health = None
        self.boss_health = None
        self.boss_health_max = None
    
    def update(self, level, time_left, fps):
        self.floor_num = level.floor_num
        self.time_left = time_left
        self.fps = fps
        
        for time in level.messages.keys():
            if not self.override_level_msg and time == self.time_left:
                self.current_message = level.messages[time]
                
    def update_boss(self, player, level):
        if level.level_type != BOSS:
            return
            
        self.player_health = player.health
        self.boss_health = level.boss.health
        self.boss_health_max = level.boss.initial_health
    
    def draw(self, display):
        msg_display = self.message_font.render(self.current_message, True, WHITE)
        msg_rect = msg_display.get_rect()
        msg_rect.topleft = (0, 550)
        display.blit(msg_display, msg_rect)
        
        floor_display = self.status_font.render("Floor %d | " % self.floor_num, True, WHITE)
        floor_rect = floor_display.get_rect()
        floor_rect.bottomleft = (0, 599)
        
        time_color = WHITE if self.time_left > 10 else RED
        time_display = self.status_font.render("Time left: %d" % self.time_left, True, time_color)
        time_rect = time_display.get_rect()
        time_rect.bottomleft = floor_rect.bottomright
        
        display.blit(floor_display, floor_rect)
        display.blit(time_display, time_rect)
        
        if self.player_health != None and self.boss_health != None:
            player_text_display = self.status_font.render("Player:", True, WHITE)
            player_text_rect = player_text_display.get_rect()
            player_text_rect.bottomleft = (250, 599)
            display.blit(player_text_display, player_text_rect)
            
            for i in range(self.player_health):
                x_offset = (i + 1) * 15
                box_xpos = player_text_rect.bottomright[0] + x_offset
                player_healthbox_rect = pygame.Rect(0, 0, 10, 20)
                # Adjust the position manually
                player_healthbox_rect.bottomleft = (box_xpos, 598)
                box_color = ORANGE if self.player_health == 1 else SKYBLUE
                pygame.draw.rect(display, box_color, player_healthbox_rect)
            
            if self.boss_health > 0:    
                boss_text_display = self.status_font.render("Nemesis:", True, WHITE)
                boss_text_rect = boss_text_display.get_rect()
                boss_text_rect.bottomleft = (390, 599)
                display.blit(boss_text_display, boss_text_rect)
                
                bar_xpos = boss_text_rect.bottomright[0] + 10
                boss_health_length = int(((self.boss_health * 1.0) / self.boss_health_max) * 100)
                boss_health_rect = pygame.Rect(0, 0, boss_health_length, 20)
                # Adjust the position manually
                boss_health_rect.bottomleft = (bar_xpos, 598)
                pygame.draw.rect(display, RED, boss_health_rect)
        
        pause_display = self.status_font.render("ESCAPE - Pause", True, WHITE)
        pause_rect = pause_display.get_rect()
        pause_rect.topright = (799, 550)
        display.blit(pause_display, pause_rect)
        
        fps_display = self.small_font.render("FPS: %.0f" % self.fps, True, WHITE)
        fps_rect = fps_display.get_rect()
        fps_rect.bottomright = (799, 599)
        display.blit(fps_display, fps_rect)
