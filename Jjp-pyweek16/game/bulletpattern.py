import math
import pygame
from random import randint
from random import choice
from random import random as randfloat
from pygame.locals import *

import units
from bullet import Bullet
from constants import *

class BulletPattern(object):
    def __init__(self):
        raise NotImplementedError()
    
    def assign_new_pos(self, boss, tick):
        raise NotImplementedError()
        
    def add_new_bullets(self, boss, player, tick):
        raise NotImplementedError()

class MovingStepsEasy(BulletPattern): # Stop, shoot, stop, repeat
    def __init__(self):
        self.dest_pos_y = None
        self.shooting = False
        self.bullets = 0
        self.frequency = 10
        
        self.possible_y = [2, 4, 6, 8, 10, 12, 14, 16, 18]
    
    def assign_new_pos(self, boss, tick):
        if self.dest_pos_y == None:
            self.dest_pos_y = units.tiles_to_pixels(choice(self.possible_y))
            
        if not self.shooting:
            if boss.pos_y < self.dest_pos_y:
                boss.pos_y += 5
            elif boss.pos_y > self.dest_pos_y:
                boss.pos_y -= 5
            else:
                self.shooting = True
                self.bullets = 5 # Shoot five bullets at a time in one place
    
    def add_new_bullets(self, boss, player, tick):
        if tick % self.frequency == 0 and self.shooting and self.bullets > 0:
            bullet_x = boss.rect.topleft[0] - BULLETSIZE
            bullet_y = boss.rect.topleft[1] + 10
                
            boss.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED * 2, 0))
            self.bullets -= 1
            
            if self.bullets == 0:
                self.dest_pos_y = units.tiles_to_pixels(choice(self.possible_y))
                self.shooting = False
                
class MovingConstantEasy(BulletPattern): # Boss is moving, straight bullets
    def __init__(self):
        self.moving_up = False
        self.frequency = 10
    
    def assign_new_pos(self, boss, tick):
        if self.moving_up:
            boss.pos_y -= 5
            if boss.pos_y < 40:
                self.moving_up = False
        else:
            boss.pos_y += 5
            if boss.pos_y > 470:
                self.moving_up = True
        
    def add_new_bullets(self, boss, player, tick):
        if tick % self.frequency == 0:
            bullet_x = boss.rect.topleft[0] - BULLETSIZE
            bullet_y = boss.rect.topleft[1] + 10
            
            boss.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED * 2, 0))
            
class MovingConstantNormal(BulletPattern): # Boss is moving, two straight bullets
    def __init__(self):
        self.moving_up = False
        self.frequency_fast = 10
        self.frequency_slow = 15
    
    def assign_new_pos(self, boss, tick):
        if self.moving_up:
            boss.pos_y -= 5
            if boss.pos_y < 40:
                self.moving_up = False
        else:
            boss.pos_y += 5
            if boss.pos_y > 470:
                self.moving_up = True
        
    def add_new_bullets(self, boss, player, tick):
        bullet_x = boss.rect.topleft[0] - BULLETSIZE
        bullet_y = boss.rect.topleft[1] + 10
        
        if tick % self.frequency_fast == 0:
            boss.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED * 2, 0))
        if tick % self.frequency_slow == 0:
            boss.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED, 0))
            
class MovingConstantHard(BulletPattern): # Boss is moving, three straight, one aimed
    def __init__(self):
        self.moving_up = False
        self.frequency_fast = 10
        self.frequency_medium = 10
        self.frequency_slow = 15
    
    def assign_new_pos(self, boss, tick):
        if self.moving_up:
            boss.pos_y -= 5
            if boss.pos_y < 40:
                self.moving_up = False
        else:
            boss.pos_y += 5
            if boss.pos_y > 470:
                self.moving_up = True
        
    def add_new_bullets(self, boss, player, tick):
        bullet_x = boss.rect.topleft[0] - BULLETSIZE
        bullet_y = boss.rect.topleft[1] + 10
        
        if tick % self.frequency_fast == 0:
            boss.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED * 3, 0))
        if tick % self.frequency_medium == 0:
            boss.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED * 2, 0))
        if tick % self.frequency_slow == 0: # Also aimed
            boss.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED, 0))
            
            target_pos = player.rect.center
            dist_x = target_pos[0] - bullet_x
            dist_y = target_pos[1] - bullet_y
            
            hyp = math.sqrt(dist_x ** 2 + dist_y ** 2)
            
            vel_x = int((BULLETSPEED * 2) * (dist_x / hyp))
            vel_y = int((BULLETSPEED * 2) * (dist_y / hyp))
            
            boss.bullets.append(Bullet(bullet_x, bullet_y, vel_x, vel_y))
            
class AimedEasy(BulletPattern): # Boss is moving, aimed bullets
    def __init__(self):
        self.moving_up = False
        self.frequency = 10
    
    def assign_new_pos(self, boss, tick):
        if self.moving_up:
            boss.pos_y -= 2
            if boss.pos_y < 40:
                self.moving_up = False
        else:
            boss.pos_y += 2
            if boss.pos_y > 470:
                self.moving_up = True
        
    def add_new_bullets(self, boss, player, tick):
        if tick % self.frequency == 0:
            bullet_x = boss.rect.topleft[0] - BULLETSIZE
            bullet_y = boss.rect.topleft[1] + 10
            
            target_pos = player.rect.center
            dist_x = target_pos[0] - bullet_x
            dist_y = target_pos[1] - bullet_y
            
            hyp = math.sqrt(dist_x ** 2 + dist_y ** 2)
            
            vel_x = int((BULLETSPEED * 2) * (dist_x / hyp))
            vel_y = int((BULLETSPEED * 2) * (dist_y / hyp))
            
            boss.bullets.append(Bullet(bullet_x, bullet_y, vel_x, vel_y))
            
class AimedNormal(BulletPattern): # Boss is moving, two aimed bullets
    def __init__(self):
        self.moving_up = False
        self.frequency_fast = 10
        self.frequency_slow = 15
    
    def assign_new_pos(self, boss, tick):
        if self.moving_up:
            boss.pos_y -= 5
            if boss.pos_y < 40:
                self.moving_up = False
        else:
            boss.pos_y += 5
            if boss.pos_y > 470:
                self.moving_up = True
        
    def add_new_bullets(self, boss, player, tick):
        if not tick % self.frequency_fast == 0 and not tick % self.frequency_slow == 0:
            return
        
        bullet_x = boss.rect.topleft[0] - BULLETSIZE
        bullet_y = boss.rect.topleft[1] + 10
        
        target_pos = player.rect.center
        dist_x = target_pos[0] - bullet_x
        dist_y = target_pos[1] - bullet_y
        
        hyp = math.sqrt(dist_x ** 2 + dist_y ** 2)
        
        aimed_x = int((BULLETSPEED * 2) * (dist_x / hyp))
        aimed_y = int((BULLETSPEED * 2) * (dist_y / hyp))
        
        if tick % self.frequency_fast == 0:
            boss.bullets.append(Bullet(bullet_x, bullet_y, aimed_x, aimed_y))
        if tick % self.frequency_slow == 0:
            boss.bullets.append(Bullet(bullet_x, bullet_y, aimed_x / 2, aimed_y / 2))

class ShotgunNormal(BulletPattern): # Boss is moving, spread pattern
    def __init__(self):
        self.moving_up = False
        self.cooldown = 30
    
    def assign_new_pos(self, boss, tick):
        if self.moving_up:
            boss.pos_y -= 5
            if boss.pos_y < 40:
                self.moving_up = False
        else:
            boss.pos_y += 5
            if boss.pos_y > 470:
                self.moving_up = True
    
    def add_new_bullets(self, boss, player, tick):
        if self.cooldown <= 0:
            bullet_x = boss.rect.topleft[0] - BULLETSIZE
            bullet_y = boss.rect.topleft[1] + 10
            
            # Spread
            for vel_y in range(-4, 5, 2):
                boss.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED * 2, vel_y))
            
            self.cooldown = 20 # 2/3 of a second
        else:
            self.cooldown -= 1
            
class ShotgunHard(BulletPattern): # Boss is moving, spread pattern and aimed bullet
    def __init__(self):
        self.moving_up = False
        self.cooldown = 30
    
    def assign_new_pos(self, boss, tick):
        if self.moving_up:
            boss.pos_y -= 5
            if boss.pos_y < 40:
                self.moving_up = False
        else:
            boss.pos_y += 5
            if boss.pos_y > 470:
                self.moving_up = True
    
    def add_new_bullets(self, boss, player, tick):
        bullet_x = boss.rect.topleft[0] - BULLETSIZE
        bullet_y = boss.rect.topleft[1] + 10
        
        if self.cooldown == 8: # Aimed
            target_pos = player.rect.center
            dist_x = target_pos[0] - bullet_x
            dist_y = target_pos[1] - bullet_y
        
            hyp = math.sqrt(dist_x ** 2 + dist_y ** 2)
        
            aimed_x = int((BULLETSPEED * 2) * (dist_x / hyp))
            aimed_y = int((BULLETSPEED * 2) * (dist_y / hyp))
            boss.bullets.append(Bullet(bullet_x, bullet_y, aimed_x, aimed_y))
            
            self.cooldown -= 1
        
        elif self.cooldown <= 0: # Spread
            for vel_y in range(-4, 5, 2):
                boss.bullets.append(Bullet(bullet_x, bullet_y, -BULLETSPEED * 2, vel_y))
            
            self.cooldown = 15 # 1/2 of a second
        else:
            self.cooldown -= 1

class Berserk(BulletPattern): # All over the place
    def __init__(self):
        self.moving_up = False
        self.frequency = 3
    
    def assign_new_pos(self, boss, tick):
        if self.moving_up:
            boss.pos_y -= 5
            if boss.pos_y < 40:
                self.moving_up = False
        else:
            boss.pos_y += 5
            if boss.pos_y > 470:
                self.moving_up = True
                
    def add_new_bullets(self, boss, player, tick):
        if tick % self.frequency == 0:
            bullet_x = boss.rect.topleft[0] - BULLETSIZE
            bullet_y = boss.rect.topleft[1] + 10
            
            aimed_x = int(-randint(BULLETSPEED, BULLETSPEED * 3) * (randfloat()))
            aimed_y = int(-randint(-BULLETSPEED, BULLETSPEED) * (randfloat()))
            
            if aimed_x == 0:
                aimed_x = -BULLETSPEED
            
            boss.bullets.append(Bullet(bullet_x, bullet_y, aimed_x, aimed_y))
