import pygame
from pygame.locals import *

import data
from constants import *

class SoundSystem(object):
    def __init__(self):
        self.can_play = True
        
        self.flags = {}
        
        self.enemy_shoot = pygame.mixer.Sound(data.filepath('enemy-shoot.wav'))
        self.boss_hit = pygame.mixer.Sound(data.filepath('boss-hit.wav'))
        self.boss_death = pygame.mixer.Sound(data.filepath('boss-death.wav'))
        self.player_shoot = pygame.mixer.Sound(data.filepath('player-shoot.wav'))
        self.player_hit = pygame.mixer.Sound(data.filepath('player-hit.wav'))
        self.tile_break = pygame.mixer.Sound(data.filepath('tile-break.wav'))
        self.level_clear = pygame.mixer.Sound(data.filepath('level-clear.wav'))
        
    def play_sounds(self):
        if not self.can_play:
            return
        
        # There's a better way, but I'm running out of time...
        if "enemy-shoot" in self.flags.keys() and self.flags["enemy-shoot"] == True:
            self.enemy_shoot.play()
        if "boss-shoot" in self.flags.keys() and self.flags["boss-shoot"] == True:
            self.enemy_shoot.play()
        if "player-hit" in self.flags.keys() and self.flags["player-hit"] == True:
            self.player_hit.play()
        if "player-shoot" in self.flags.keys() and self.flags["player-shoot"] == True:
            self.player_shoot.play()
        if "boss-hit" in self.flags.keys() and self.flags["boss-hit"] == True:
            self.boss_hit.play()
        if "boss-death" in self.flags.keys() and self.flags["boss-death"] == True:
            self.boss_death.play()
        if "tile-break" in self.flags.keys() and self.flags["tile-break"] == True:
            self.tile_break.play()
        if "level-clear" in self.flags.keys() and self.flags["level-clear"] == True:
            self.level_clear.play()
            
        for sound in self.flags.keys():
            self.flags[sound] = False
        
    def play_level_music(self, level):
        pass
