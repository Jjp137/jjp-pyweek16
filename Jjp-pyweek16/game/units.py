import pygame
from pygame.locals import *

from constants import *

# Anything that has to do with unit conversion goes here.

def tiles_to_pixels(x):
    return x * TILESIZE + BORDERSIZE

def pixels_to_tiles(x):
    return (x - BORDERSIZE) / TILESIZE

def get_tile_rect(tile_x, tile_y):
    pixel_x = tiles_to_pixels(tile_x)
    pixel_y = tiles_to_pixels(tile_y)
    return pygame.Rect(pixel_x, pixel_y, TILESIZE, TILESIZE)
