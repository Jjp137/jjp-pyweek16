Evil Squared
============

Entry in PyWeek #16  <http://www.pyweek.org/16/>
URL: http://pyweek.org/e/Jjp-pyweek16
Team: Jjp-Pyweek16
Members: Jjp137
License: see LICENSE.txt

Dependencies
------------

This game requires Python 2.7 and PyGame 1.9.1 to run.

It is untested on Python 3 and may not work. Older versions of Python 2 may
not work as well.

Story
-----

You are a secret agent, and you are trying to stop your evil nemesis, Dr. Cube,
in his evil tower. You must climb the floors of his tower and stop him because 
he's planning something evil!

...or just play the game :p

Running the Game
----------------

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Othewise open a terminal / console and "cd" to the game directory and run:

  python run_game.py

There are no command line parameters.

How to Play the Game
--------------------

Move around with the arrow keys. Hold SHIFT to run. Running is important!

During boss fights, hold SPACE to fire bullets.

Each floor/level has a time limit. You lose the level when time runs out. You 
have an unlimited number of tries per level. Press ENTER to retry the level.

You also lose if you run into a pit or a bullet. Orange floors turn into pits 
after a moment, so be careful!

To complete a level, get to the goal, which is the yellow square.

Boss floors are special. You have three bars of health when you are on those 
floors, so getting hit by one or two bullets will not cause you to lose on 
those floors.

Defeating your nemesis on boss floors is required to reach the goal.

Some floors are very hard. Don't give up!

Credits
-------

Jjp137 wrote the code for the game and designed the levels.

data.py and __init__.py is part of Skellington 2.3.

There are no graphic files; everything is drawn using Rect objects.
Maybe next time...

The FreeUniversal font was taken from: 
http://openfontlibrary.org/en/font/freeuniversal

Sounds generated from the Bfxr tool, which can be found here:
http://www.bfxr.net/

See LICENSE.txt for licensing information.

Special Thanks
--------------

First of all, special thanks to the PyWeek organizers for organizing this 
event. It was fun!

The #pyweek IRC channel was also interesting to read throughout the competition,
even if I didn't talk much, so thank you to everyone in there for that :)

Also, special thanks to all my friends who have supported me throughout the
years. Honorable mention goes to my friend Kenzo for the (horrible) plot and 
title :p

...and finally, thank you for playing! :D

